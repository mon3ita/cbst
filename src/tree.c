#include "include/tree.h"

tree * new_tree() {
    tree * root = (tree*)malloc(2 * sizeof(tree *) + 2 * sizeof(int));
    if(root == NULL) {
        printf("Error while allocation memory for new tree.");
        return NULL;
    }

    root->left = root->right = NULL;

    return root;
}

void insert_into(tree * root, int data) {
    if(!root->size) {
        root->data = data;
        return;
    } else if(!root->left && root->data >= data) {
        root->left = new_tree();
        root->left->data = data;
        root->left->left = root->left->right = NULL;
        root->left->size++;
        return;
    } else if(!root->right && root->data < data) {
        root->right = new_tree();
        root->right->data = data;
        root->right->left = root->right->right = NULL;
        root->right->size++;
        return;
    }

    root = root->data >= data ? root->left : root->right;
    insert_into(root, data);
}

void set_size(tree * root) {
   root->size++;
}

void _print(tree * root, int level, char * tab) {
    if(!root) {
        return;
    }

    tree * left, * right;

    if(level > 0) {
        tab = _tabs(level);
    }

    left = root->left;
    right = root->right;

    if(tab && tab != "")
        printf("%sRoot: %d ", tab, root->data);
    else
        printf("Root: %d ", root->data);

    if(left) {
        printf(">Left %d ", left->data);
    } else {
        printf(">Left {} ");
    }

    if(right) {
        printf(">Right %d ", right->data);
    } else {
        printf("> Right {} ");
    }

    printf("\n");

    if(tab && tab != "") free(tab);

    _print(left, level + 1, tab);
    _print(right, level + 1, tab);
}

void print(tree * root) {
    _print(root, 0, "");
}

char * _tabs(int size) {
    char * tab = (char*)malloc(size * sizeof(char*) + 1);

    int i = 0;
    while(i < size) {
        tab[i++] = '\t';
    }

    tab[size] = '\0';
    return tab;
}

void remove_from(tree * root, int data) {

    if(root->data == data && root->left && root->right) {
        tree * min_right = _get_min(root->right);
        root->data = min_right->left->data;
        free(min_right->left);
        min_right->left = min_right->left->right;
        root->size--;
        return;
    }

    
    if(root->data == data && !root->right) {
        tree * old = root;
        root = root->left;
        root->size = old->size - 1;
        free(old);
        root->size--;
        return;
    }

    if(root->data == data && !root->left) {
        tree * old = root;
        root = root->right;
        root->size = old->size - 1;
        free(old);
        return;
    }

    if(root->left && root->left->data == data && !root->left->left && !root->left->right) {
        free(root->left);
        root->left = NULL;
        return;
    }

    if(root->right && root->right->data == data && !root->right->left && !root->right->right) {
        free(root->right);
        root->right = NULL;
        root->size--;
        return;
    }

    root = root->data > data ? root->left : root->right;
    remove_from(root, data);
}

tree * _get_min(tree * root) {
    if(!root->left->left) return root;
    return _get_min(root->left);
}

void free_tree(tree * root) {
    if(!root)
        return;

    tree * left = root->left;
    tree * right = root->right;

    free(root);

    free_tree(left);
    free_tree(right);
}