#ifndef _TREE_H
#define _TREE_H

#include <stdio.h>
#include <stdlib.h>

typedef struct tree {
    int data;
    int size;
    struct tree * left, * right;
} tree;

tree * new_tree();
void insert_into(tree *, int);
void remove_from(tree *, int);
tree * _get_min(tree *);
void _print(tree *, int, char *);
void print(tree *);
void set_size(tree *);
void free_tree(tree *);

char * _tabs(int);

#endif