#include <stdio.h>
#include <stdlib.h>

#include "src/include/tree.h"

int main(void) {

    //EXAMPLE USAGE
    tree * root = new_tree();

    tree * p = root;
    for(int i = 0; i < 10; i++) {
        if(i % 2 == 0)
            insert_into(p, i * 2);
        else
            insert_into(p, i / 2);
        set_size(root);

        p = root;
    }

    printf("%d\n", root->size);

    tree * pTree = root;
    print(pTree);

    //remove_from(pTree, 0);
    //print(pTree);

    return 0;
}